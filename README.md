# Tubo-Redirect-Userscript
Simple Userscript to redirect YouTube to Tubo.


Install it on GreasyFork [here](https://greasyfork.org/en/scripts/472138-tubo-redirect)


You can learn more about Tubo [here](https://github.com/migalmoreno/tubo).
